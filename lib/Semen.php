<?php
	
	class Semen extends Funcoes
		{
		private $atributos;
		
		public function __construct()
			{
			}
			
		public function __destruct()
			{
			}
			
		public function __set(string $atributo, $valor)
	    	{
	      $this->atributos[$atributo] = $valor;
	      return $this;
	    	}
	
	   public function __get(string $atributo)
	   	{
	      return $this->atributos[$atributo];
	    	}
	
	   public function __isset($atributo)
	    	{
	      return isset($this->atributos[$atributo]);
	    	}
	   
	   /**
	     * Salvar o contato
	     * @return boolean
	     */
	   public function save($modo)
    		{
        	$colunas = $this->preparar($this->atributos);
        	if ($modo == 0) 
        		{
            $query = "INSERT INTO tb_semen (".
                implode(', ', array_keys($colunas)).
                ") VALUES (".
                implode(', ', array_values($colunas)).");";
        		} 
        	else 
        		{
            foreach ($colunas as $key => $value) 
            	{
               if ($key !== 'idtb_semen') 
               	{
                  $definir[] = "{$key}={$value}";
                	}
            	}
            $query = "UPDATE tb_semen SET ".implode(', ', $definir)." WHERE idtb_semen={$this->idtb_semen};";
        		}
        	echo ($query);
        	if ($conexao = Conexao::getInstance()) 
        		{
            $stmt = $conexao->prepare($query);
            if ($stmt->execute()) 
            	{
               return $stmt->rowCount();
            	}
        		}
        	return false;
    		}

    	/**
       * Retorna uma lista de contatos
       * @return array/boolean
       */
    	public static function all($pagina=false,$limite=false)
    		{
        	$conexao = Conexao::getInstance();
        	$sql = "SELECT * FROM tb_semen order by idtb_semen";
        	if ($pagina)
        		{
        		$offset = $limite*($pagina-1);
        		$sql .= " limit $offset,$limite "; 
        		}
        	//echo ($sql);
        	$stmt    = $conexao->prepare($sql);
        	$result  = array();
        	if ($stmt->execute()) 
        		{
            while ($rs = $stmt->fetchObject(Semen::class)) 
            	{
               $result[] = $rs;
            	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}
    		
    	public static function allClientes($pagina=false,$limite=false)
    		{
        	$conexao = Conexao::getInstance();
        	$sql = "SELECT c.idtb_cliente, c.nome_cliente FROM tb_cliente c inner join tb_semen s on c.idtb_cliente=s.tb_cliente_idtb_cliente group by c.idtb_cliente, c.nome_cliente order by c.nome_cliente ";
        	if ($pagina)
        		{
        		$offset = $limite*($pagina-1);
        		$sql .= " limit $offset,$limite "; 
        		}
        	//echo ($sql);
        	$stmt    = $conexao->prepare($sql);
        	$result  = array();
        	if ($stmt->execute()) 
        		{
            while ($rs = $stmt->fetchObject(Cliente::class)) 
            	{
               $result[] = $rs;
            	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}

    	/**
       * Retornar o número de registros
       * @return int/boolean
       */
    	public static function count()
    		{
        	$conexao = Conexao::getInstance();
        	$count   = $conexao->prepare("SELECT count(*) FROM tb_semen;");
        	if ($count->execute()) 
        		{
        		$dd = $count->fetchAll();
            return (int) $dd[0][0];
        		}
        	return false;
    		}

    	/**
       * Encontra um recurso pelo id
       * @param type $id
       * @return type
       */
    	public static function find($id)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT * FROM tb_semen WHERE idtb_semen={$id};";
        	//echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute()) 
        		{
            if ($stmt->rowCount() > 0) 	
            	{
               $resultado = $stmt->fetchObject(Semen::class);
               if ($resultado) 	
               	{
                  return $resultado;
                	}
            	}
        		}
        	return false;
    		}
    		
    	public static function findSemem($id)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "select * from tb_semen  WHERE tb_cliente_idtb_cliente={$id}";
        	//echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	$result  = array();
        	if ($stmt->execute()) 
        		{
            while ($rs = $stmt->fetchObject(Semen::class)) 
            	{
               $result[] = $rs;
            	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}

    	/**
       * Destruir um recurso
       * @param type $id
       * @return boolean
       * Rever esse método questão: temporiedade 
       */
    	public static function destroy($id)
    		{
        	$conexao = Conexao::getInstance();
        	if ($conexao->exec("DELETE FROM tb_semen WHERE idtb_semen={$id};")) 
        		{
            return true;
        		}
        	return false;
    		}
		};
?>