<?php

	require_once("lib/HTML/Template/Sigma.php");
	
	class Visao extends HTML_Template_Sigma
		{
		private $geral;
		function __construct()
			{
			$this->geral['teste'] = "Testado";
			parent::__construct("modelos","cache");
			}
			
		function __destruct()
			{
			$this->show();
			}
			
		public function __set(string $atributo, $valor)
    		{
        	$this->geral[$atributo] = $valor;
        	return $this;
    		}

    	public function __get(string $atributo)
    		{
        	return $this->geral[$atributo];
    		}

    	public function __isset($atributo)
    		{
        	return isset($this->geral[$atributo]);
    		}
			
		public function visualizar($template="index")
			{
			$this->loadTemplateFile($template.".ajp");
			//print_r($this->geral);
			$this->setVariable($this->geral);
			}
		};
?>