<?php
	
	class Aspiracao extends Funcoes
		{
		private $atributos;
		
		public function __construct()
			{
			}
			
		public function __destruct()
			{
			}
			
		public function __set(string $atributo, $valor)
	    	{
	      $this->atributos[$atributo] = $valor;
	      return $this;
	    	}
	
	   public function __get(string $atributo)
	   	{
	      return $this->atributos[$atributo];
	    	}
	
	   public function __isset($atributo)
	    	{
	      return isset($this->atributos[$atributo]);
	    	}
	   
	   /**
	     * Salvar o contato
	     * @return boolean
	     */
	   public function save($modo)
    		{
    		//Modificar para atender as formas corretas da aspiração
        	$colunas = $this->preparar($this->atributos);
        	print_r($colunas);
        	if ($modo == 0) 
        		{
            $query = "INSERT INTO tb_aspiracao (".
                implode(', ', array_keys($colunas)).
                ") VALUES (".
                implode(', ', array_values($colunas)).");";
        		} 
        	else 
        		{
            foreach ($colunas as $key => $value) 
            	{
               if ($key !== 'idtb_aspiracao') 
               	{
                  $definir[] = "{$key}={$value}";
                	}
            	}
            $query = "UPDATE tb_aspiracao SET ".implode(', ', $definir)." WHERE idtb_aspiracao={$this->idtb_Aspiracao};";
        		}
        	echo ($query);
        	if ($conexao = Conexao::getInstance()) 
        		{
            $stmt = $conexao->prepare($query);
            if ($stmt->execute()) 
            	{
            	if ($modo == 0)
            		{
            		$this->idtb_aspiracao = $conexao->lastInsertId();
            		}
               return $stmt->rowCount();
            	}
        		}
        	return false;
    		}

    	/**
       * Retorna uma lista de contatos
       * @return array/boolean
       */
    	public static function all($pagina=false,$limite=false)
    		{
        	$conexao = Conexao::getInstance();
        	$sql = "SELECT * FROM tb_aspiracao order by idtb_aspiracao";
        	if ($pagina)
        		{
        		$offset = $limite*($pagina-1);
        		$sql .= " limit $offset,$limite "; 
        		}
        	//echo ($sql);
        	$stmt    = $conexao->prepare($sql);
        	$result  = array();
        	if ($stmt->execute()) 
        		{
            while ($rs = $stmt->fetchObject(Aspiracao::class)) 
            	{
               $result[] = $rs;
            	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}

    	/**
       * Retornar o número de registros
       * @return int/boolean
       */
    	public static function count()
    		{
        	$conexao = Conexao::getInstance();
        	$count   = $conexao->prepare("SELECT count(*) FROM tb_aspiracao;");
        	if ($count->execute()) 
        		{
        		$dd = $count->fetchAll();
            return (int) $dd[0][0];
        		}
        	return false;
    		}

    	/**
       * Encontra um recurso pelo id
       * @param type $id
       * @return type
       */
    	public static function find($id)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT * FROM tb_aspiracao WHERE idtb_aspiracao=?";
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute([$id])) 
        		{
            return $stmt->fetchObject(Aspiracao::class);
        		}
        	return false;
    		}
    	
    	/**
       * Encontra um recurso pelo id
       * @param type $id
       * @return type
       */
    	public static function find_list($id,$pagina=false,$limite=false)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT * FROM tb_aspiracao ";
        	if ($id != 0)
        		{
        		$consulta .= "WHERE idtb_aspiracao={$id};";
        		}
        	if ($pagina)
        		{
        		$offset = $limite*($pagina-1);
        		$consulta .= " limit $offset,$limite "; 
        		}
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute()) 
        		{
            if ($stmt->rowCount() > 0) 	
            	{
            	while ($rs = $stmt->fetchObject(Aspiracao::class)) 
	            	{
	               $resultado[] = $rs;
	            	}
               if ($resultado) 	
               	{
                  return $resultado;
                	}
            	}
        		}
        	return false;
    		}
    		
    	/**
       * Destruir um recurso
       * @param type $id
       * @return boolean
       * Rever esse método questão: temporiedade 
       */
    	public static function destroy($id)
    		{
        	$conexao = Conexao::getInstance();
        	if ($conexao->exec("DELETE FROM tb_aspiracao WHERE idtb_aspiracao={$id};")) 
        		{
            return true;
        		}
        	return false;
    		}
		};
?>