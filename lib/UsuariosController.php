<?php

	class UsuariosController extends Controller
		{

    	/**
     	 * Lista de Usuários
       */
    	public function listar()
    		{
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
        	$contatos = Usuario::all();
       	$visao = new Visao();
       	$visao->visualizar("grade");
       	$visao->setVariable("titulo","Usuarios del Sistema");
       	$visao->setCurrentBlock("USUARIOS");
       	if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}
       	foreach ($contatos as $contato) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("codigo",$contato->login);
            $visao->setVariable("name",$contato->name);
            $visao->setVariable("email",$contato->email);
            $valor = "No";
            if (strcmp(trim($contato->priv_admin),"Y") == 0)
            	{
            	$valor = "Sí";
            	}
            $visao->setVariable("priv_admin",$valor);
            if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       			{
	            $visao->setVariable("ativo","");
	       		}
	       	else 
	       		{
	       		$visao->setVariable("ativo","style=\"visibility:hidden;\"");
	       		} 
           	$visao->parseCurrentBlock("DADOS");          	
            }
         $visao->parseCurrentBlock("USUARIOS");
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("form_usuario");
       	$visao->setVariable("titulo","Registro de usuarios del sistema"); 
         $visao->setVariable("acao","salvar");
         $visao->setCurrentBlock("SENHA_NEW");
         $visao->setVariable("pswd","");
         $visao->parseCurrentBlock("SENHA_NEW");
         $visao->setCurrentBlock("ADMINISTRADOR");
         $visao->setVariable("a_valor","Y");
         $visao->setVariable("a_selecionado","");
         $visao->setVariable("a_texto","Sí");
         $visao->parseCurrentBlock("ADMINISTRADOR");
         $visao->setCurrentBlock("ADMINISTRADOR");
         $visao->setVariable("a_valor","");
         $visao->setVariable("a_selecionado","CHECKED");
         $visao->setVariable("a_texto","No");
         $visao->parseCurrentBlock("ADMINISTRADOR");
        	//return $this->view('form');
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->id;
        	$contato = Usuario::find($id);
			$visao = new Visao();
       	$visao->visualizar("form_usuario");
       	$visao->setVariable("titulo","Actualización de datos de usuario"); 
       	$visao->setVariable("acao","atualizar");
         $visao->setVariable("login",$contato->login);
         $visao->setVariable("name",$contato->name);
         $visao->setVariable("email",$contato->email);
         $visao->setCurrentBlock("SENHA_EDIT");
         $visao->setVariable("pswd","Cambio");
         $visao->parseCurrentBlock("SENHA_EDIT");
         $admin[0] = array ('a_valor'=>'Y','a_texto'=> 'Sí');
         $admin[1] = array ('a_valor'=>'','a_texto'=> 'No');
         for ($i=0; $i < count($admin); $i++)
         	{
         	//echo ("id $i".strlen($contato->priv_admin)." ".strlen($admin[$i]['a_texto']));
         	$visao->setCurrentBlock("ADMINISTRADOR");
         	$visao->setVariable("a_valor",$admin[$i]['a_valor']);
         	if (strlen(trim($contato->priv_admin)) == strlen(trim($admin[$i]['a_valor'])) )
         		{
         		$visao->setVariable("a_selecionado","CHECKED");
         		}
         	else 
         		{
         		$visao->setVariable("a_selecionado","");
         		}
         	$visao->setVariable("a_texto",$admin[$i]['a_texto']);
         	$visao->parseCurrentBlock("ADMINISTRADOR");
         	}
         //return $this->view('form', ['contato' => $contato]);
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
        	$contato           = new Usuario;
        	$contato->login = $this->request->login;
        	$contato->pswd = md5($this->request->pswd);
        	$contato->name     = $this->request->name;
        	$contato->email    = $this->request->email;
        	$contato->priv_admin = $this->request->priv_admin;
        	$contato->active = "Y";
        	if ($contato->save(0)) 
        		{
            return $this->listar();
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
        	$id                = $this->request->login;
        	$contato           = Usuario::find($id);
        	$contato->login = $this->request->login;
        	$contato->name     = $this->request->name;
        	$contato->email    = $this->request->email;
        	$contato->priv_admin = $this->request->priv_admin;
        	$contato->save(1);

        	return $this->listar();
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->login;
        	$contato = Usuario::destroy($id);
        	return $this->listar();
    		}
    		
    	/**
    	 * Verificar acesso ao Sistema
    	 * @param dados mixed
    	 * @return bool
    	 */
    	public function logar()
    		{
    		$login = $this->request->login;
    		$senha = md5($this->request->pswd);
    		$usuario = Usuario::find($login, $senha);
    		if ($usuario)
    			{
    			session_unset();
				$_SESSION['login'] = 1;
				$_SESSION['usuario'] = serialize($usuario);
				//var_dump($_SESSION);
				}
			else 
				{
				session_unset();
				$_SESSION['login'] = 0;
				}
			header("Location: index.php");			    		
    		}
    		
    	public function logout()
    		{
    		session_unset();
			$_SESSION['login'] = 0;
			header("Location: index.php");
    		}
		};