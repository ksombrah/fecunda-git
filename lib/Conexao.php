<?php
	/**
	 * Arquivo: Conexao.php
	 * @package Default
	 * @sub-package Classe de Conexão com o Banco de dados
	 * @page-level 1
	 */

require_once("parametros.inc");

	/**
	 * Classe de Conexão com o Banco de dados
	 * @package Default
	 * @return void
	 */
class Conexao
	{
	/**
	 * Ponteiro de conexão com o banco de dados
	 * @access private
	 * @var mixed
	 */
   private static $conexao;

	/**
	 * Construtor da Classe CONEXAO
	 * @return void
	 */
   private function __construct()
    	{
    	}
    	
    /**
	 * Destrutor da Classe CONEXAO
	 * @return void
	 */
   private function __destruct()
   	{
   	}

	/**
	 * Método public utilizado para retornar a conexão com o banco de dados.\n
	 * @return mixed
	 */
	public static function getInstance()
    	{
      if (is_null(self::$conexao)) 
      	{
      	try 
      		{
      		$options = array(
    			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
				); 
         	self::$conexao = new \PDO(str_conection,usuario,senha,$options);
         	self::$conexao->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
         	//self::$conexao->exec('set names utf8');
         	}
         catch (PDOException $e) 
				{
				die('Connection failed: ' . $e->getMessage());
				}
       	}
      return self::$conexao;
    	}
};