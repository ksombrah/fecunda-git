<?php

	class DoadorasController extends Controller
		{

    	/**
     	 * Lista de Usuários
       */
    	public function listar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
        	$doadoras = Doadora::all($pagina,$limite);
       	$visao = new Visao();
       	$visao->visualizar("lista_doadora");
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
       	$i=0;
       	foreach ($doadoras as $doadora) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("arp_doadora",$doadora->arp_doadora);
            $visao->setVariable("rp_doadora",$doadora->rp_doadora);
            $propriedade = Propriedade::find($doadora->tb_propriedade_idtb_propriedade);
            $visao->setVariable("propriedade",$propriedade->nome_propriedade);
            $raca = Raca::find($doadora->tb_raca_idtb_raca);
            $visao->setVariable("raca",$raca->nome_raca);
            $visao->setVariable("idtb_doadora",$doadora->idtb_doadora);
           	$visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Doadora::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Doadora::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("cadastro_doadora");
       	$visao->setVariable("acao","salvar");
         $propriedades = Propriedade::all();
         foreach ($propriedades as $propriedade)
         	{
         	$visao->setCurrentBlock("PROPRIEDADES");
         	$visao->setVariable("idtb_propriedade",$propriedade->idtb_propriedade);
         	$visao->setVariable("selecionado","");
         	$visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
         	$visao->parseCurrentBlock("PROPRIEDADES");
         	}
         $racas = Raca::all();
         foreach ($racas as $raca)
         	{
         	$visao->setCurrentBlock("RACAS");
         	$visao->setVariable("idtb_raca",$raca->idtb_raca);
         	$visao->setVariable("selecionada","");
         	$visao->setVariable("nome_raca",$raca->nome_raca);
         	$visao->parseCurrentBlock("RACAS");
         	}
         $visao->setVariable("acao_txt","Registrar");
        	//return $this->view('form');
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->idtb_doadora;
        	$doadora = Doadora::find($id);
			$visao = new Visao();
       	$visao->visualizar("cadastro_doadora");
       	$visao->setVariable("acao","atualizar");
       	$visao->setVariable("idtb_doadora",$doadora->idtb_doadora);
       	$propriedades = Propriedade::all();
         foreach ($propriedades as $propriedade)
         	{
         	$visao->setCurrentBlock("PROPRIEDADES");
         	$visao->setVariable("idtb_propriedade",$propriedade->idtb_propriedade);
         	if ($doadora->tb_propriedade_idtb_propriedade == $propriedade->idtb_propriedade)
         		{
         		$visao->setVariable("selecionado","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionado","");
         		}
         	$visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
         	$visao->parseCurrentBlock("PROPRIEDADES");
         	}
       	$visao->setVariable("arp_doadora",$doadora->arp_doadora);
       	$visao->setVariable("rp_doadora",$doadora->rp_doadora);
         $racas = Raca::all();
         foreach ($racas as $raca)
         	{
         	$visao->setCurrentBlock("RACAS");
         	$visao->setVariable("idtb_raca",$raca->idtb_raca);
         	if ($doadora->tb_raca_idtb_raca == $raca->idtb_raca)
         		{
         		$visao->setVariable("selecionada","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionada","");
         		}
         	$visao->setVariable("nome_raca",$raca->nome_raca);
         	$visao->parseCurrentBlock("RACAS");
         	}
         $visao->setVariable("acao_txt","Actualizar");
         //return $this->view('form', ['contato' => $contato]);
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
        	$doadora           = new Doadora;
        	$doadora->arp_doadora = $this->request->arp_doadora;
        	$doadora->rp_doadora = $this->request->rp_doadora;
        	$doadora->tb_propriedade_idtb_propriedade = $this->request->tb_propriedade_idtb_propriedade;
        	$doadora->tb_raca_idtb_raca = $this->request->tb_raca_idtb_raca;
        	if ($doadora->save(0)) 
        		{
            return $this->listar();
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
        	$id                = $this->request->idtb_doadora;
        	$doadora           =  Doadora::find($id);
        	$doadora->arp_doadora = $this->request->arp_doadora;
        	$doadora->rp_doadora = $this->request->rp_doadora;
        	$doadora->tb_propriedade_idtb_propriedade = $this->request->tb_propriedade_idtb_propriedade;
        	$doadora->tb_raca_idtb_raca = $this->request->tb_raca_idtb_raca;
        	$doadora->save(1);

        	return $this->listar();
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->idtb_doadora;
        	$contato = Doadora::destroy($id);
        	return $this->listar();
    		}
    		
		};