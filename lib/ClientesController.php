<?php

	class ClientesController extends Controller
		{

    	/**
     	 * Lista de Clientes
       */
    	public function listar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
        	$clientes = Cliente::all();
       	$visao = new Visao();
       	$visao->visualizar("lista_cliente");
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
       	$i=0;
       	foreach ($clientes as $cliente) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("nome_cliente",$cliente->nome_cliente);
            $visao->setVariable("telefone_cliente",$cliente->telefone_cliente);
            $visao->setVariable("endereco_cliente",$cliente->endereco_cliente);
            $visao->setVariable("email_cliente",$cliente->email_cliente);
            $visao->setVariable("idtb_cliente",$cliente->idtb_cliente);
           	$visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Cliente::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Cliente::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("cadastro_cliente");
       	$visao->setVariable("acao","salvar");
       	$visao->setVariable("acao_txt","Registrar");
        	//return $this->view('form');
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->idtb_cliente;
        	$cliente = Cliente::find($id);
			$visao = new Visao();
       	$visao->visualizar("cadastro_cliente"); 
       	$visao->setVariable("acao","atualizar");
         $visao->setVariable("idtb_cliente",$cliente->idtb_cliente);
         $visao->setVariable("nome_cliente",$cliente->nome_cliente);
         $visao->setVariable("endereco_cliente",$cliente->endereco_cliente);
         $visao->setVariable("telefone_cliente",$cliente->telefone_cliente);
         $visao->setVariable("email_cliente",$cliente->email_cliente);
         $visao->setVariable("senha_cliente",$cliente->senha_cliente);
         $visao->setVariable("acao_txt","Actualizar");
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
        	$cliente           = new Cliente;
        	$cliente->nome_cliente = $this->request->nome_cliente;
        	$cliente->telefone_cliente = $this->request->telefone_cliente;
        	$cliente->endereco_cliente = $this->request->endereco_cliente;
        	$cliente->email_cliente = $this->request->email_cliente;
        	$cliente->senha_cliente = md5($this->request->senha_cliente);
        	if ($cliente->save(0)) 
        		{
            return $this->listar();
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
        	$id                = $this->request->idtb_cliente;
        	$cliente           = Cliente::find($id);
        	$cliente->idtb_cliente = $this->request->idtb_cliente;
        	$cliente->nome_cliente = $this->request->nome_cliente;
        	$cliente->telefone_cliente = $this->request->telefone_cliente;
        	$cliente->endereco_cliente = $this->request->endereco_cliente;
        	$cliente->email_cliente = $this->request->email_cliente;
        	if (strcmp($cliente->senha_cliente,$this->request->senha_cliente) != 0)
        		{
        		$cliente->senha_cliente = md5($this->request->senha_cliente);
        		}
        	$cliente->save(1);

        	return $this->listar();
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->idtb_cliente;
        	$cliente = Cliente::destroy($id);
        	return $this->listar();
    		}
    		
    	/**
    	 * Verificar acesso ao Sistema
    	 * @param dados mixed
    	 * @return bool
    	 */
    	public function logar()
    		{
    		$login = $this->request->login;
    		$senha = md5($this->request->pswd);
    		$usuario = Usuario::find($login, $senha);
    		if ($usuario)
    			{
    			session_unset();
				$_SESSION['login'] = 1;
				$_SESSION['usuario'] = serialize($usuario);
				//var_dump($_SESSION);
				}
			else 
				{
				session_unset();
				$_SESSION['login'] = 0;
				}
			header("Location: index.php");			    		
    		}
    		
    	public function logout()
    		{
    		session_unset();
			$_SESSION['login'] = 0;
			header("Location: index.php");
    		}
		};