<?php

	class PropriedadesController extends Controller
		{

    	/**
     	 * Lista de Usuários
       */
    	public function listar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
    		//var_dump($acesso);
        	$propriedades = Propriedade::all();
       	$visao = new Visao();
       	$visao->visualizar("lista_propriedade");
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
       	$i=0;
       	foreach ($propriedades as $propriedade) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
            $visao->setVariable("endereco_propriedade",$propriedade->endereco_propriedade);
            $visao->setVariable("localidade_propriedade",$propriedade->localidade_propriedade);
            $visao->setVariable("departamento_propriedade",$propriedade->departamento_propriedade);
            //$visao->setVariable("email_propriedade",$propriedade->email_propriedade);
            $visao->setVariable("telefone_propriedade",$propriedade->telefone_propriedade);
            $cliente = Cliente::find($propriedade->tb_cliente_idtb_cliente);
            $visao->setVariable("cliente",$cliente->nome_cliente);
            $visao->setVariable("idtb_propriedade",$propriedade->idtb_propriedade);
           	$visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Propriedade::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Propriedade::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("cadastro_propriedade"); 
         $visao->setVariable("acao","salvar");
         $visao->setVariable("acao_txt","Registrar");
         $clientes = Cliente::all();
         foreach ($clientes as $cliente)
         	{
         	$visao->setCurrentBlock("CLIENTES");
         	$visao->setVariable("idtb_cliente",$cliente->idtb_cliente);
         	$visao->setVariable("selecionado","");
         	$visao->setVariable("nome_cliente",$cliente->nome_cliente);
         	$visao->parseCurrentBlock("CLIENTES");
         	}
        	//return $this->view('form');
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->idtb_propriedade;
        	$propriedade = Propriedade::find($id);
			$visao = new Visao();
       	$visao->visualizar("cadastro_propriedade"); 
       	$visao->setVariable("acao","atualizar");
       	$visao->setVariable("idtb_propriedade",$propriedade->idtb_propriedade);
       	$visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
       	$visao->setVariable("endereco_propriedade",$propriedade->endereco_propriedade);
       	$visao->setVariable("localidade_propriedade",$propriedade->localidade_propriedade);
       	$visao->setVariable("departamento_propriedade",$propriedade->departamento_propriedade);
       	$visao->setVariable("email_propriedade",$propriedade->email_propriedade);
       	$visao->setVariable("telefone_propriedade",$propriedade->telefone_propriedade);
         $clientes = Cliente::all();
         foreach ($clientes as $cliente)
         	{
         	$visao->setCurrentBlock("CLIENTES");
         	$visao->setVariable("idtb_cliente",$cliente->idtb_cliente);
         	if ($propriedade->tb_cliente_idtb_cliente == $cliente->idtb_cliente)
         		{
         		$visao->setVariable("selecionado","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionado","");
         		}
         	$visao->setVariable("nome_cliente",$cliente->nome_cliente);
         	$visao->parseCurrentBlock("CLIENTES");
         	}
         $visao->setVariable("acao_txt","Actualizar");
         //return $this->view('form', ['contato' => $contato]);
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
        	$propriedade           = new Propriedade;
        	$propriedade->nome_propriedade = $this->request->nome_propriedade;
        	$propriedade->endereco_propriedade = $this->request->endereco_propriedade;
        	$propriedade->localidade_propriedade = $this->request->localidade_propriedade;
        	$propriedade->departamento_propriedade = $this->request->departamento_propriedade;
        	$propriedade->email_propriedade = $this->request->email_propriedade;
        	$propriedade->telefone_propriedade = $this->request->telefone_propriedade;
        	$propriedade->tb_cliente_idtb_cliente = $this->request->tb_cliente_idtb_cliente;
        	if ($propriedade->save(0)) 
        		{
            return $this->listar();
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
        	$id                = $this->request->idtb_propriedade;
        	$propriedade       = Propriedade::find($id);
        	$propriedade->idtb_propriedade = $this->request->idtb_propriedade;
        	$propriedade->nome_propriedade = $this->request->nome_propriedade;
        	$propriedade->endereco_propriedade = $this->request->endereco_propriedade;
        	$propriedade->localidade_propriedade = $this->request->localidade_propriedade;
        	$propriedade->departamento_propriedade = $this->request->departamento_propriedade;
        	$propriedade->email_propriedade = $this->request->email_propriedade;
        	$propriedade->telefone_propriedade = $this->request->telefone_propriedade;
        	$propriedade->tb_cliente_idtb_cliente = $this->request->tb_cliente_idtb_cliente;
        	$propriedade->save(1);

        	return $this->listar();
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->idtb_propriedade;
        	$propriedade = Propriedade::destroy($id);
        	return $this->listar();
    		}
    		
		};