<?php

	class TourosController extends Controller
		{

    	/**
     	 * Lista de Usuários
       */
    	public function listar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
        	$touros = Touro::all($pagina,$limite);
       	$visao = new Visao();
       	$visao->visualizar("lista_touro");
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
       	$i=0;
       	foreach ($touros as $touro) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("nome_touro",$touro->nome_touro);
            $raca = Raca::find($touro->tb_raca_idtb_raca);
            $visao->setVariable("raca",$raca->nome_raca);
            $visao->setVariable("idtb_touro",$touro->idtb_touro);
           	$visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Touro::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Touro::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("cadastro_touro");
       	$visao->setVariable("acao","salvar");
       	$racas = Raca::all();
         foreach ($racas as $raca)
         	{
         	$visao->setCurrentBlock("RACAS");
         	$visao->setVariable("idtb_raca",$raca->idtb_raca);
         	$visao->setVariable("selecionada","");
         	$visao->setVariable("nome_raca",$raca->nome_raca);
         	$visao->parseCurrentBlock("RACAS");
         	}
         $visao->setVariable("acao_txt","Registrar");
        	//return $this->view('form');
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->idtb_touro;
        	$touro = Touro::find($id);
			$visao = new Visao();
       	$visao->visualizar("cadastro_touro");
       	$visao->setVariable("acao","atualizar");
       	$visao->setVariable("idtb_touro",$touro->idtb_touro);
       	$visao->setVariable("nome_touro",$touro->nome_touro);
       	$racas = Raca::all();
         foreach ($racas as $raca)
         	{
         	$visao->setCurrentBlock("RACAS");
         	$visao->setVariable("idtb_raca",$raca->idtb_raca);
         	if ($touro->tb_raca_idtb_raca == $raca->idtb_raca)
         		{
         		$visao->setVariable("selecionada","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionada","");
         		}
         	$visao->setVariable("nome_raca",$raca->nome_raca);
         	$visao->parseCurrentBlock("RACAS");
         	}
         $visao->setVariable("acao_txt","Actualizar");
         //return $this->view('form', ['contato' => $contato]);
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
        	$touro           = new Touro;
        	$touro->nome_touro = $this->request->nome_touro;
        	$touro->tb_raca_idtb_raca = $this->request->tb_raca_idtb_raca;
        	if ($touro->save(0)) 
        		{
            return $this->listar();
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
        	$id                = $this->request->idtb_touro;
        	$touro           = Touro::find($id);
        	$touro->idtb_touro = $this->request->idtb_touro;
        	$touro->nome_touro = $this->request->nome_touro;
        	$touro->tb_raca_idtb_raca = $this->request->tb_raca_idtb_raca;
        	$touro->save(1);

        	return $this->listar();
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->id;
        	$touro = Touro::destroy($id);
        	return $this->listar();
    		}
    		
		};