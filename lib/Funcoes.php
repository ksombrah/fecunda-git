<?php

class Funcoes
	{
	/**
     * Tornar valores aceitos para sintaxe SQL
     * @param type $dados
     * @return string
     */
	protected function escapar($dados)
    	{
    	if (is_integer($dados))
    		{
    		return $dados;
    		}
      elseif (is_string($dados) & !empty($dados)) 
      	{
         return "'".addslashes($dados)."'";
        	} 
      elseif (is_bool($dados)) 
      	{
         return $dados ? 'TRUE' : 'FALSE';
        	} 
      elseif ($dados !== '') 
      	{
         return $dados;
        	} 
      else 
      	{
         return 'NULL';
        	}
    	}

    /**
     * Verifica se dados são próprios para ser salvos
     * @param array $dados
     * @return array
     */
   protected function preparar($dados)
    	{
      $resultado = array();
      foreach ($dados as $k => $v) 
      	{
      	if (is_integer($v))
      		{
      		$resultado[$k] = $v;
      		}
         elseif (is_scalar($v)) 
         	{
            $resultado[$k] = $this->escapar($v);
            }
        	}
      return $resultado;
    	}
	};
?>