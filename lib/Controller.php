<?php

class Controller
{
    public $request;
    private $visao;

    public function __construct()
    	{
      $this->request = new Request;
    	}
    public function __destruct()
    	{

    	}

    public function view($arquivo, $array = null)
    	{
    	//$this->visao =  new Visao();
      if (!is_null($array)) 
      	{
         foreach ($array as $var => $value) 
         	{
            ${$var} = $value;
            }
        	}
      ob_start();
      include "{$arquivo}.php";
      ob_flush();
      //$this->visao->visualizar("{$arquivo}.ajp");
    	}
	}