<?php

	class TransferenciaController extends Controller
		{

    	/**
     	 * Lista de Transferencias
       */
    	public function listar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
        	$Aspiracoes = Aspiracao::all($pagina,$limite);
       	$visao = new Visao();
       	$visao->visualizar("lista_transferencia_vitro");
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
       	$i=0;
       	foreach ($Aspiracoes as $aspiracao) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("idtb_aspiracao",$aspiracao->idtb_aspiracao);
            $visao->setVariable("hora_inicio_aspiracao",$aspiracao->hora_inicio_aspiracao);
            $visao->setVariable("hora_fim_aspiracao",$aspiracao->hora_fim_aspiracao);
            $propriedade = Propriedade::find($aspiracao->tb_propriedade_idtb_propriedade);
            $visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
            $visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Semen::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Aspiracao::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}
    		
    	/**
     	 * Lista de Usuários
       */
    	public function visualizar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
    		$visao = new Visao();
    		if ($this->request->idtb_aspiracao)
    			{
	        	$doadoras = AspiracaoItens::all($pagina,$limite,$this->request->idtb_aspiracao);
	       	$visao->visualizar("lista_trans_vitro_visualizacao");
	       	$visao->setVariable("pagina",$pagina);
	       	for ($l = 0; $l < count($limites); $l++)
	       		{
	       		$visao->setCurrentBlock("LIMITES");
	       		$visao->setVariable("op_limite",$limites[$l]);
	         	if ($limites[$l] == $limite)
	         		{
	         		$visao->setVariable("selected","selected");
	         		}
	         	else 
	         		{
	         		$visao->setVariable("selected","");
	         		}
	            $visao->parseCurrentBlock("LIMITES");
	       		}
	       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
	       		{
	       		$visao->setVariable("ativon","");
	       		}
	       	else 
	       		{
	       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
	       		}*/
	       	$i=0;
	       	foreach ($doadoras as $aspiracao) 
	            {
	            $visao->setCurrentBlock("DADOS");
	            $visao->setVariable("tb_aspiracao_idtb_aspiracao",$aspiracao->tb_aspiracao_idtb_aspiracao);
	            $doadora = Doadora::find($aspiracao->tb_doadora_idtb_doadora);
	            $visao->setVariable("arp_doadora",$doadora->arp_doadora);
	            $touro = Touro::find($aspiracao->tb_touro_idtb_touro);
	            $visao->setVariable("nome_touro",$touro->nome_touro);
	            $visao->setVariable("semem_aspiracao",$aspiracao->semem_aspiracao);
	            $visao->parseCurrentBlock("DADOS");
	           	$i++;          	
	            }
	         $visao->setVariable("primeiro",1);
	         $visao->setVariable("contagem",$i);
	         $visao->setVariable("total",AspiracaoItens::count($this->request->idtb_aspiracao));
	         $ant = $pagina-1;
	         if ($ant < 1)
	         	{
	         	$visao->setVariable("disabled_a","disabled");
	         	$ant = 1;
	         	}
	         else 
	         	{
	         	$visao->setVariable("disabled_a","");
	         	}
	         $visao->setVariable("pagina_a",$ant);
	         $visao->setVariable("limite_a",$limite);
	         $total_p = ceil(AspiracaoItens::count($this->request->idtb_aspiracao)/$limite);
	         for ($p=1; $p <= $total_p; $p++)
	         	{
	         	$visao->setCurrentBlock("PAGINAS");
	         	if ($p == $pagina)
	         		{
	         		$visao->setVariable("active","active");
	         		}
	         	else 
	         		{
	         		$visao->setVariable("active","");
	         		}
	            $visao->setVariable("pagina_n",$p);
	            $visao->setVariable("limite_n",$limite);
	            $visao->parseCurrentBlock("PAGINAS");
	         	}
	         if ($pagina < $total_p)
	         	{
	         	$prox = $pagina+1;
	         	$visao->setVariable("disabled_p","");
	         	}
	         else 
	         	{
	         	$prox = $total_p;
	         	$visao->setVariable("disabled_p","disabled");
	         	}
	         $visao->setVariable("pagina_p",$prox);
	         $visao->setVariable("limite_p",$limite);
	         }
	      else 
	      	{
	      	$visao->visualizar("no_registros");
        		}
    		}
    		
    	/**
     	 * Lista de Usuários
       */
    	public function detalhes()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
    		$visao = new Visao();
    		if ($this->request->idtb_fiv)
    			{
	        	$transferencias = Transferencia::all($pagina,$limite,$this->request->idtb_fiv);
	       	$visao->visualizar("lista_detalhes_trans_vitro_visualizacao");
	       	$visao->setVariable("pagina",$pagina);
	       	$visao->setVariable("tb_fiv_idtb_fiv",$this->request->idtb_fiv);
	       	for ($l = 0; $l < count($limites); $l++)
	       		{
	       		$visao->setCurrentBlock("LIMITES");
	       		$visao->setVariable("op_limite",$limites[$l]);
	         	if ($limites[$l] == $limite)
	         		{
	         		$visao->setVariable("selected","selected");
	         		}
	         	else 
	         		{
	         		$visao->setVariable("selected","");
	         		}
	            $visao->parseCurrentBlock("LIMITES");
	       		}
	       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
	       		{
	       		$visao->setVariable("ativon","");
	       		}
	       	else 
	       		{
	       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
	       		}*/
	       	$i=0;
	       	foreach ($transferencias as $aspiracao) 
	            {
	            $visao->setCurrentBlock("DADOS");
	            $visao->setVariable("idtb_transferencia_vidro",$aspiracao->idtb_transferencia_vidro);
	            $visao->setVariable("embriao_transferencia_vidro",$aspiracao->embriao_transferencia_vidro);
	            $visao->setVariable("dia_transferencia_vidro",$aspiracao->dia_transferencia_vidro);
	            $visao->setVariable("cl_transferencia_vidro",$aspiracao->cl_transferencia_vidro);
	            $visao->setVariable("dificuldade_transferencia_vidro",$aspiracao->dificuldade_transferencia_vidro);
	            $visao->setVariable("receptora_transferencia_vidro",$aspiracao->receptora_transferencia_vidro);
	            $visao->setVariable("trinta_dias_transferencia_vidro",$aspiracao->trinta_dias_transferencia_vidro);
	            $visao->setVariable("sessenta_dias_transferencia_vidro",$aspiracao->sessenta_dias_transferencia_vidro);
	            $visao->parseCurrentBlock("DADOS");
	           	$i++;          	
	            }
	         $visao->setVariable("primeiro",1);
	         $visao->setVariable("contagem",$i);
	         $visao->setVariable("total",Transferencia::count($this->request->idtb_fiv));
	         $ant = $pagina-1;
	         if ($ant < 1)
	         	{
	         	$visao->setVariable("disabled_a","disabled");
	         	$ant = 1;
	         	}
	         else 
	         	{
	         	$visao->setVariable("disabled_a","");
	         	}
	         $visao->setVariable("pagina_a",$ant);
	         $visao->setVariable("limite_a",$limite);
	         $total_p = ceil(Transferencia::count($this->request->idtb_fiv)/$limite);
	         for ($p=1; $p <= $total_p; $p++)
	         	{
	         	$visao->setCurrentBlock("PAGINAS");
	         	if ($p == $pagina)
	         		{
	         		$visao->setVariable("active","active");
	         		}
	         	else 
	         		{
	         		$visao->setVariable("active","");
	         		}
	            $visao->setVariable("pagina_n",$p);
	            $visao->setVariable("limite_n",$limite);
	            $visao->parseCurrentBlock("PAGINAS");
	         	}
	         if ($pagina < $total_p)
	         	{
	         	$prox = $pagina+1;
	         	$visao->setVariable("disabled_p","");
	         	}
	         else 
	         	{
	         	$prox = $total_p;
	         	$visao->setVariable("disabled_p","disabled");
	         	}
	         $visao->setVariable("pagina_p",$prox);
	         $visao->setVariable("limite_p",$limite);
	         }
	      else 
	      	{
	      	$visao->visualizar("no_registros");
        		}
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("cadastro_transferencia_vitro");
       	$visao->setVariable("acao","salvar");
       	
        	//return $this->view('form');
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->id;
        	$Aspiracao = Semen::find($id);
			$visao = new Visao();
       	$visao->visualizar("cadastro_Aspiracao");
       	$visao->setVariable("acao","salvar");
       	$visao->setVariable("idtb_semen",$Aspiracao->idtb_semen);
       	$visao->setVariable("tb_qntidade_Aspiracao",$Aspiracao->tb_qntidade_Aspiracao);
       	$touros = Touro::all();
         foreach ($touros as $touro)
         	{
         	$visao->setCurrentBlock("TOUROS");
         	$visao->setVariable("idtb_touro",$touro->idtb_touro);
         	if ($Aspiracao->tb_touro_idtb_touro == $touro->idtb_touro)
         		{
         		$visao->setVariable("selecionado","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionado","");
         		}
         	$visao->setVariable("nome_touro",$touro->nome_touro);
         	$visao->parseCurrentBlock("TOUROS");
         	}
         $clientes = Cliente::all();
         foreach ($clientes as $cliente)
         	{
         	$visao->setCurrentBlock("CLIENTES");
         	$visao->setVariable("idtb_cliente",$cliente->idtb_cliente);
         	if ($Aspiracao->tb_cliente_idtb_cliente == $cliente->idtb_cliente)
         		{
         		$visao->setVariable("selecionados","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionados","");
         		}
         	$visao->setVariable("nome_cliente",$cliente->nome_cliente);
         	$visao->parseCurrentBlock("CLIENTES");
         	}
         //return $this->view('form', ['contato' => $contato]);
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
        	$Aspiracao           = new Semen;
        	$Aspiracao->tb_touro_idtb_touro = $this->request->tb_touro_idtb_touro;
        	$Aspiracao->tb_cliente_idtb_cliente = $this->request->tb_cliente_idtb_cliente;
        	$Aspiracao->tb_qntidade_Aspiracao = $this->request->tb_qntidade_Aspiracao;
        	if ($Aspiracao->save(0)) 
        		{
            return $this->listar();
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
        	$id                = $this->request->idtb_semen;
        	$Aspiracao           = Semen::find($id);
        	$Aspiracao->idtb_semen = $this->request->idtb_semen;
        	$Aspiracao->tb_touro_idtb_touro = $this->request->tb_touro_idtb_touro;
        	$Aspiracao->tb_cliente_idtb_cliente = $this->request->tb_cliente_idtb_cliente;
        	$Aspiracao->tb_qntidade_Aspiracao = $this->request->tb_qntidade_Aspiracao;
        	$Aspiracao->save(1);

        	return $this->listar();
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->id;
        	$Aspiracao = Semen::destroy($id);
        	return $this->listar();
    		}

		};