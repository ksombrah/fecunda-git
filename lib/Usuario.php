<?php
	
	class Usuario extends Funcoes
		{
		private $atributos;
		
		public function __construct()
			{
			}
			
		public function __destruct()
			{
			}
			
		public function __set(string $atributo, $valor)
	    	{
	      $this->atributos[$atributo] = $valor;
	      return $this;
	    	}
	
	   public function __get(string $atributo)
	   	{
	      return $this->atributos[$atributo];
	    	}
	
	   public function __isset($atributo)
	    	{
	      return isset($this->atributos[$atributo]);
	    	}
	   
	   /**
	     * Salvar o contato
	     * @return boolean
	     */
	   public function save($modo)
    		{
        	$colunas = $this->preparar($this->atributos);
        	if ($modo == 0) 
        		{
            $query = "INSERT INTO sec_loginusers (".
                implode(', ', array_keys($colunas)).
                ") VALUES (".
                implode(', ', array_values($colunas)).");";
        		} 
        	else 
        		{
            foreach ($colunas as $key => $value) 
            	{
               if ($key !== 'login') 
               	{
                  $definir[] = "{$key}={$value}";
                	}
            	}
            $query = "UPDATE sec_loginusers SET ".implode(', ', $definir)." WHERE login='{$this->login}';";
        		}
        	echo ($query);
        	if ($conexao = Conexao::getInstance()) 
        		{
            $stmt = $conexao->prepare($query);
            if ($stmt->execute()) 
            	{
               return $stmt->rowCount();
            	}
        		}
        	return false;
    		}

    	/**
       * Retorna uma lista de contatos
       * @return array/boolean
       */
    	public static function all()
    		{
        	$conexao = Conexao::getInstance();
        	$stmt    = $conexao->prepare("SELECT * FROM sec_loginusers;");
        	$result  = array();
        	if ($stmt->execute()) 
        		{
            while ($rs = $stmt->fetchObject(Usuario::class)) 
            	{
               $result[] = $rs;
            	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}

    	/**
       * Retornar o número de registros
       * @return int/boolean
       */
    	public static function count()
    		{
        	$conexao = Conexao::getInstance();
        	$count   = $conexao->exec("SELECT count(*) FROM sec_loginusers;");
        	if ($count) 
        		{
            return (int) $count;
        		}
        	return false;
    		}

    	/**
       * Encontra um recurso pelo id
       * @param type $id
       * @return type
       */
    	public static function find()
    		{
        	$conexao = Conexao::getInstance();
        	if (func_num_args() == 1)
        		{
        		$consulta = "SELECT * FROM sec_loginusers WHERE login='".func_get_arg(0)."';";
        		}
        	else if (func_num_args() == 2)
        		{
        		$consulta = "SELECT * FROM sec_loginusers WHERE login='".func_get_arg(0)."' and pswd='".func_get_arg(1)."' and active='Y';";
        		}
        	else 
        		{
        		return false;
        		}
        	echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute()) 
        		{
            if ($stmt->rowCount() > 0) 	
            	{
               $resultado = $stmt->fetchObject('Usuario');
               if ($resultado) 	
               	{
                  return $resultado;
                	}
            	}
        		}
        	return false;
    		}

    	/**
       * Destruir um recurso
       * @param type $id
       * @return boolean
       * Rever esse método questão: temporiedade 
       */
    	public static function destroy($id)
    		{
        	$conexao = Conexao::getInstance();
        	/* Opção de exclusão total
        	if ($conexao->exec("DELETE FROM sec_loginusers WHERE login='{$id}';")) 
        		{
            return true;
        		}
        	*/
        	if ($conexao->exec("UPDATE sec_loginusers SET active='' WHERE login='{$id}';"))
        		{
        		return true;
        		}
        	return false;
    		}
		};
?>