<?php

	class SemensController extends Controller
		{

    	/**
     	 * Lista de Usuários
       */
    	public function listar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
        	$semens = Semen::allClientes($pagina,$limite);
       	$visao = new Visao();
       	$visao->visualizar("lista_semem");
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
       	$i=0;
       	foreach ($semens as $semen) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("titulo",$semen->nome_cliente);
            $visao->setVariable("idtb_cliente",$semen->idtb_cliente);
            //$itens = Semen::findSemem($semen->idtb_cliente);
            /*foreach ($itens as $item) Ajusta listagem
            	{
            	$visao->setCurrentBlock("ITEM");
            	$touro = Touro::find($item->tb_touro_idtb_touro);
            	$visao->setVariable("touro",$touro->nome_touro);
            	$visao->setVariable("tb_qntidade_semem",$item->tb_qntidade_semem);
            	$visao->setVariable("idtb_semen",$item->idtb_semen);
            	$visao->parseCurrentBlock("ITEM");
            	}*/
            $visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Semen::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Semen::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}
    		
    	/**
     	 * Lista de Usuários
       */
    	public function listarItens()
    		{
       	$visao = new Visao();
       	$visao->visualizar("lista_semem_itens");
         $itens = Semen::findSemem($this->request->idtb_cliente);
         foreach ($itens as $item)
         	{
         	$visao->setCurrentBlock("ITEM");
         	$touro = Touro::find($item->tb_touro_idtb_touro);
         	$visao->setVariable("touro",$touro->nome_touro);
         	$visao->setVariable("tb_qntidade_semem",$item->tb_qntidade_semem);
         	$visao->setVariable("idtb_semen",$item->idtb_semen);
         	$visao->parseCurrentBlock("ITEM");
         	}
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("cadastro_semem");
       	$visao->setVariable("acao","salvar");
       	$touros = Touro::all();
         foreach ($touros as $touro)
         	{
         	$visao->setCurrentBlock("TOUROS");
         	$visao->setVariable("idtb_touro",$touro->idtb_touro);
         	$visao->setVariable("selecionado","");
         	$visao->setVariable("nome_touro",$touro->nome_touro);
         	$visao->parseCurrentBlock("TOUROS");
         	}
         $clientes = Cliente::all();
         foreach ($clientes as $cliente)
         	{
         	$visao->setCurrentBlock("CLIENTES");
         	$visao->setVariable("idtb_cliente",$cliente->idtb_cliente);
         	$visao->setVariable("selecionados","");
         	$visao->setVariable("nome_cliente",$cliente->nome_cliente);
         	$visao->parseCurrentBlock("CLIENTES");
         	}
         $visao->setVariable("acao_txt","Registrar");
        	//return $this->view('form');
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->id;
        	$semem = Semen::find($id);
			$visao = new Visao();
       	$visao->visualizar("cadastro_semem");
       	$visao->setVariable("acao","atualizar");
       	$visao->setVariable("idtb_semen",$semem->idtb_semen);
       	$visao->setVariable("tb_qntidade_semem",$semem->tb_qntidade_semem);
       	$touros = Touro::all();
         foreach ($touros as $touro)
         	{
         	$visao->setCurrentBlock("TOUROS");
         	$visao->setVariable("idtb_touro",$touro->idtb_touro);
         	if ($semem->tb_touro_idtb_touro == $touro->idtb_touro)
         		{
         		$visao->setVariable("selecionado","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionado","");
         		}
         	$visao->setVariable("nome_touro",$touro->nome_touro);
         	$visao->parseCurrentBlock("TOUROS");
         	}
         $clientes = Cliente::all();
         foreach ($clientes as $cliente)
         	{
         	$visao->setCurrentBlock("CLIENTES");
         	$visao->setVariable("idtb_cliente",$cliente->idtb_cliente);
         	if ($semem->tb_cliente_idtb_cliente == $cliente->idtb_cliente)
         		{
         		$visao->setVariable("selecionados","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionados","");
         		}
         	$visao->setVariable("nome_cliente",$cliente->nome_cliente);
         	$visao->parseCurrentBlock("CLIENTES");
         	}
         $visao->setVariable("acao_txt","Actualizar");
         //return $this->view('form', ['contato' => $contato]);
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
        	$semem           = new Semen;
        	$semem->tb_touro_idtb_touro = $this->request->tb_touro_idtb_touro;
        	$semem->tb_cliente_idtb_cliente = $this->request->tb_cliente_idtb_cliente;
        	$semem->tb_qntidade_semem = $this->request->tb_qntidade_semem;
        	if ($semem->save(0)) 
        		{
            return $this->listar();
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
        	$id                = $this->request->idtb_semen;
        	$semem           = Semen::find($id);
        	$semem->idtb_semen = $this->request->idtb_semen;
        	$semem->tb_touro_idtb_touro = $this->request->tb_touro_idtb_touro;
        	$semem->tb_cliente_idtb_cliente = $this->request->tb_cliente_idtb_cliente;
        	$semem->tb_qntidade_semem = $this->request->tb_qntidade_semem;
        	$semem->save(1);

        	return $this->listar();
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->id;
        	$semem = Semen::destroy($id);
        	return $this->listar();
    		}

		};