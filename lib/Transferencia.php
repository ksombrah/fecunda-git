<?php
	
	class Transferencia extends Funcoes
		{
		private $atributos;
		
		public function __construct()
			{
			}
			
		public function __destruct()
			{
			}
			
		public function __set(string $atributo, $valor)
	    	{
	      $this->atributos[$atributo] = $valor;
	      return $this;
	    	}
	
	   public function __get(string $atributo)
	   	{
	      return $this->atributos[$atributo];
	    	}
	
	   public function __isset($atributo)
	    	{
	      return isset($this->atributos[$atributo]);
	    	}
	   
	   /**
	     * Salvar o contato
	     * @return boolean
	     */
	   public function save($modo)
    		{
        	$colunas = $this->preparar($this->atributos);
        	if ($modo == 0) 
        		{
            $query = "INSERT INTO tb_transferencia_vidro (".
                implode(', ', array_keys($colunas)).
                ") VALUES (".
                implode(', ', array_values($colunas)).");";
        		} 
        	else 
        		{
            foreach ($colunas as $key => $value) 
            	{
               if ($key !== 'idtb_transferencia_vidro') 
               	{
                  $definir[] = "{$key}={$value}";
                	}
            	}
            $query = "UPDATE tb_transferencia_vidro SET ".implode(', ', $definir)." WHERE idtb_transferencia_vidro={$this->idtb_transferencia_vidro};";
        		}
        	echo ($query);
        	if ($conexao = Conexao::getInstance()) 
        		{
            $stmt = $conexao->prepare($query);
            if ($stmt->execute()) 
            	{
               return $stmt->rowCount();
            	}
        		}
        	return false;
    		}

    	/**
       * Retorna uma lista de contatos
       * @return array/boolean
       */
    	public static function all($pagina=false,$limite=false,$id=0)
    		{
        	$conexao = Conexao::getInstance();
        	$sql = "SELECT * FROM tb_transferencia_vidro ";
        	if ($id != 0)
        		{
        		$sql .= " where tb_fiv_idtb_fiv=".$id;
        		}
        	$sql .= " order by idtb_transferencia_vidro";
        	if ($pagina)
        		{
        		$offset = $limite*($pagina-1);
        		$sql .= " limit $offset,$limite "; 
        		}
        	//echo ($sql);
        	$stmt    = $conexao->prepare($sql);
        	$result  = array();
        	if ($stmt->execute()) 
        		{
            while ($rs = $stmt->fetchObject(Transferencia::class)) 
            	{
               $result[] = $rs;
            	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}
    		

    	/**
       * Retornar o número de registros
       * @return int/boolean
       */
    	public static function count($id=0)
    		{
        	$conexao = Conexao::getInstance();
        	$sql = "SELECT count(*) FROM tb_transferencia_vidro ";
        	if ($id != 0)
        		{
        		$sql .= " where tb_fiv_idtb_fiv=".$id;
        		}
        	$count   = $conexao->prepare($sql);
        	if ($count->execute()) 
        		{
        		$dd = $count->fetchAll();
            return (int) $dd[0][0];
        		}
        	return false;
    		}

    	/**
       * Encontra um recurso pelo id
       * @param type $id
       * @return type
       */
    	public static function find($id)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT * FROM tb_transferencia_vidro WHERE idtb_transferencia_vidro={$id};";
        	//echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute()) 
        		{
            if ($stmt->rowCount() > 0) 	
            	{
               $resultado = $stmt->fetchObject(Transferencia::class);
               if ($resultado) 	
               	{
                  return $resultado;
                	}
            	}
        		}
        	return false;
    		}
    		
    	/**
       * Destruir um recurso
       * @param type $id
       * @return boolean
       * Rever esse método questão: temporiedade 
       */
    	public static function destroy($id)
    		{
        	$conexao = Conexao::getInstance();
        	if ($conexao->exec("DELETE FROM tb_transferencia_vidro WHERE idtb_transferencia_vidro={$id};")) 
        		{
            return true;
        		}
        	return false;
    		}
		};
?>