<?php
	
	class AspiracaoItens extends Funcoes
		{
		private $atributos;
		
		public function __construct()
			{
			}
			
		public function __destruct()
			{
			}
			
		public function __set(string $atributo, $valor)
	    	{
	      $this->atributos[$atributo] = $valor;
	      return $this;
	    	}
	
	   public function __get(string $atributo)
	   	{
	      return $this->atributos[$atributo];
	    	}
	
	   public function __isset($atributo)
	    	{
	      return isset($this->atributos[$atributo]);
	    	}
	   
	   /**
	     * Salvar o contato
	     * @return boolean
	     */
	   public function save($modo)
    		{
    		//Modificar para atender as formas corretas da aspiração
        	$colunas = $this->preparar($this->atributos);
        	if ($modo == 0) 
        		{
            $query = "INSERT INTO tb_doadora_touro (".
                implode(', ', array_keys($colunas)).
                ") VALUES (".
                implode(', ', array_values($colunas)).");";
        		} 
        	else 
        		{
            foreach ($colunas as $key => $value) 
            	{
               if ($key !== 'idtb_doadora_touro') 
               	{
                  $definir[] = "{$key}={$value}";
                	}
            	}
            $query = "UPDATE tb_doadora_touro SET ".implode(', ', $definir)." WHERE idtb_doadora_touro={$this->idtb_doadora_touro};";
        		}
        	echo ($query);
        	if ($conexao = Conexao::getInstance()) 
        		{
            $stmt = $conexao->prepare($query);
            if ($stmt->execute()) 
            	{
               return $stmt->rowCount();
            	}
        		}
        	return false;
    		}

    	/**
       * Retorna uma lista de contatos
       * @return array/boolean
       */
    	public static function all($pagina=false,$limite=false,$id=0)
    		{
        	$conexao = Conexao::getInstance();
        	$sql = "SELECT * FROM tb_doadora_touro";
        	if ($id != 0)
        		{
        		$sql.= " where tb_aspiracao_idtb_aspiracao={$id}";
        		}
        	$sql.= " order by idtb_doadora_touro";
        	if ($pagina)
        		{
        		$offset = $limite*($pagina-1);
        		$sql .= " limit $offset,$limite "; 
        		}
        	//echo ($sql);
        	$stmt    = $conexao->prepare($sql);
        	$result  = array();
        	if ($stmt->execute()) 
        		{
            while ($rs = $stmt->fetchObject(AspiracaoItens::class)) 
            	{
               $result[] = $rs;
            	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}

    	/**
       * Retornar o número de registros
       * @return int/boolean
       */
    	public static function count($id=0)
    		{
        	$conexao = Conexao::getInstance();
        	$sql = "SELECT count(*) FROM tb_doadora_touro";
        	if ($id != 0)
        		{
        		$sql.= " where tb_aspiracao_idtb_aspiracao={$id}";
        		}
        	$count   = $conexao->prepare($sql);
        	if ($count->execute()) 
        		{
        		$dd = $count->fetchAll();
            return (int) $dd[0][0];
        		}
        	return false;
    		}

    	/**
       * Encontra um recurso pelo id
       * @param type $id
       * @return type
       */
    	public static function find($id)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT * FROM tb_doadora_touro WHERE idtb_doadora_touro={$id};";
        	//echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute()) 
        		{
            if ($stmt->rowCount() > 0) 	
            	{
               $resultado = $stmt->fetchObject(AspiracaoItens::class);
               if ($resultado) 	
               	{
                  return $resultado;
                	}
            	}
        		}
        	return false;
    		}
    		
    	/**
       * Busca itens de uma Aspiração
       * @param type $idtb_aspiracao
       * @return type
       */
    	public static function find_itens($idtb_aspiracao)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT * FROM tb_doadora_touro WHERE tb_aspiracao_idtb_aspiracao=?";
        	//echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	$result  = array();
        	if ($stmt->execute([$idtb_aspiracao])) 
        		{
	         while ($rs = $stmt->fetchObject(AspiracaoItens::class))
	         	{
	         	$result[] = $rs;
	         	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}
    		
    	/**
       * Somar dados pelo id da aspiracao
       * @param type $id
       * @return type
       */
    	public static function sum_aspiracao($id)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT sum(ocitos_viaveis_aspiracao) as ocitos_viaveis_aspiracao, ";
        	$consulta .= "sum(ocitos_grau_I_aspiracao) as ocitos_grau_I_aspiracao, ";
        	$consulta .= "sum(ocitos_grau_II_aspiracao) as ocitos_grau_II_aspiracao, ";
        	$consulta .= "sum(ocitos_grau_III_aspiracao) as ocitos_grau_III_aspiracao, ";
        	$consulta .= "sum(ocitos_viaveis_aspiracao) as ocitos_viaveis_aspiracao, ";
        	$consulta .= "sum(ocitos_nao_viaveis_aspiracao) as ocitos_nao_viaveis_aspiracao, ";
        	$consulta .= "sum(total_ocitos_aspiracao) as total_ocitos_aspiracao ";
        	$consulta .=" FROM tb_doadora_touro WHERE tb_aspiracao_idtb_aspiracao={$id};";
        	//echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute()) 
        		{
            if ($stmt->rowCount() > 0) 	
            	{
               $resultado = $stmt->fetchObject(AspiracaoItens::class);
               if ($resultado) 	
               	{
                  return $resultado;
                	}
            	}
        		}
        	return false;
    		}
    		
    	/**
       * Destruir um recurso
       * @param type $id
       * @return boolean
       * Rever esse método questão: temporiedade 
       */
    	public static function destroy($id)
    		{
        	$conexao = Conexao::getInstance();
        	if ($conexao->exec("DELETE FROM tb_doadora_touro WHERE idtb_doadora_touro={$id};")) 
        		{
            return true;
        		}
        	return false;
    		}
		};
?>