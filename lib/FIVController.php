<?php

	class FIVController extends Controller
		{

    	/**
     	 * Lista de Usuários
       */
    	public function listar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
        	$aspiracao = Aspiracao::all($pagina,$limite);
       	$visao = new Visao();
       	$visao->visualizar("lista_fiv");
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
       	$i=0;
       	foreach ($aspiracao as $fiv) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("idtb_aspiracao",$fiv->idtb_aspiracao);
            $visao->setVariable("hora_inicio_aspiracao",$fiv->hora_inicio_aspiracao);
            $visao->setVariable("hora_fim_aspiracao",$fiv->hora_fim_aspiracao);
            $propriedade = Propriedade::find($fiv->tb_propriedade_idtb_propriedade);
            $visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
            $visao->setVariable("idtb_fiv",$fiv->idtb_aspiracao);
            $visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Aspiracao::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Aspiracao::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}
    		
    	public function listarfiv()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
        	$fiv = FIV::find($this->request->idtb_fiv);
       	$visao = new Visao();
       	$visao->visualizar("lista_fiv_visualizacao");
       	$visao->setVariable("numero",$fiv->idtb_fiv);
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
 /*      	$i=0;
       	foreach ($aspiracao as $fiv) 
            {*/
         $visao->setVariable("codigo",$fiv->idtb_fiv);
         $visao->setCurrentBlock("DADOS");
         $visao->setVariable("idtb_fiv",$fiv->idtb_fiv);
         $doadora_touro = AspiracaoItens::find($fiv->tb_doadora_touro_idtb_doadora_touro);
         $doadora = Doadora::find($doadora_touro->tb_doadora_idtb_doadora);             
         $visao->setVariable("arp_doadora",$doadora->arp_doadora);
         $visao->setVariable("total_civ_fiv",$fiv->total_civ_fiv);
         $visao->setVariable("clivados_fiv",$fiv->clivados_fiv);
         $visao->setVariable("mor_fiv",$fiv->mor_fiv);
         $visao->setVariable("bi_fiv",$fiv->bi_fiv);
         $visao->setVariable("bl_fiv",$fiv->bl_fiv);
         $visao->setVariable("bx_fiv",$fiv->bx_fiv);
         $visao->setVariable("total_embrioes_fiv",$fiv->total_embrioes_fiv);
         $visao->setVariable("percentual_fiv",$fiv->percentual_fiv);
         $visao->setVariable("te_fiv",$fiv->te_fiv);
         $visao->setVariable("vitr_fiv",$fiv->vitr_fiv);
         $visao->setVariable("data_fechamento_fiv",$fiv->data_fechamento_fiv);
         $visao->parseCurrentBlock("DADOS");
           	/*$i++;          	
            }*/
          $i=1;
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",FIV::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(FIV::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("cadastro_fiv");
       	$visao->setVariable("acao","salvar");
       	if ($this->request->idtb_fiv)
       		{
       		$aspiracao = $this->request->idtb_fiv;
       		$dados = Aspiracao::find($aspiracao);
       		$visao->setVariable("aspiracao",$dados->idtb_aspiracao);
       		$dados_asp = AspiracaoItens::sum_aspiracao($dados->idtb_aspiracao);
       		$visao->setVariable("ocitos_grau_I_aspiracao",$dados_asp->ocitos_grau_I_aspiracao);
       		$visao->setVariable("ocitos_grau_II_aspiracao",$dados_asp->ocitos_grau_II_aspiracao);
       		$visao->setVariable("ocitos_grau_III_aspiracao",$dados_asp->ocitos_grau_III_aspiracao);
       		$visao->setVariable("ocitos_grau_III_aspiracao",$dados_asp->ocitos_grau_III_aspiracao);
       		$visao->setVariable("ocitos_viaveis_aspiracao",$dados_asp->ocitos_viaveis_aspiracao);
       		$visao->setVariable("ocitos_nao_viaveis_aspiracao",$dados_asp->ocitos_nao_viaveis_aspiracao);
       		$visao->setVariable("total_ocitos_aspiracao",$dados_asp->total_ocitos_aspiracao);
       		$visao->setVariable("idtb_aspiracao",$dados->idtb_aspiracao);
       		}
       	$visao->setVariable("acao_txt","Registrar");
    		}
    		
    	public function listar_fiv()
    		{
    		$visao = new Visao();
       	$visao->visualizar("cadastro_fiv_itens");
       	if ($this->request->idtb_aspiracao)
       		{
	    		$dados_fiv = AspiracaoItens::find_itens($this->request->idtb_aspiracao);
	    		$i=1;
	    		if ($dados_fiv)
	    			{
		    		foreach ($dados_fiv as $item) 
		         	{
		         	$visao->setCurrentBlock("DADOS");
		         	$visao->setVariable("controle",$i);
		         	$visao->setVariable("idtb_fiv",$this->request->idtb_aspiracao);
		         	$visao->setVariable("idtb_doadora_touro",$item->idtb_doadora_touro);
		         	$dd_fiv = FIV::registro($this->request->idtb_aspiracao,$item->idtb_doadora_touro);
		         	if ($dd_fiv)
		         		{
		         		$visao->setVariable("acaoi","atualizar");
		         		$visao->setVariable("acaotxt","Actualizar");
		         		$visao->setVariable("total_civ_fiv",$dd_fiv->total_civ_fiv);
		         		$visao->setVariable("clivados_fiv",$dd_fiv->clivados_fiv);
		         		$visao->setVariable("mor_fiv",$dd_fiv->mor_fiv);
		         		$visao->setVariable("bi_fiv",$dd_fiv->bi_fiv);
		         		$visao->setVariable("bl_fiv",$dd_fiv->bl_fiv);
		         		$visao->setVariable("bx_fiv",$dd_fiv->bx_fiv);
		         		$visao->setVariable("total_embrioes_fiv",$dd_fiv->total_embrioes_fiv);
		         		$visao->setVariable("percentual_fiv",$dd_fiv->percentual_fiv);
		         		$visao->setVariable("te_fiv",$dd_fiv->te_fiv);
		         		$visao->setVariable("vitr_fiv",$dd_fiv->vitr_fiv);
		         		$visao->setVariable("data_fechamento_fiv",$dd_fiv->data_fechamento_fiv);
		         		}
		         	else 
		         		{
		         		$visao->setVariable("acaoi","salvar");
		         		$visao->setVariable("acaotxt","Registrar");
		         		}
		         	$visao->parseCurrentBlock("DADOS");
		         	$i++;
		         	}
		         }
	         }
    		}
    		
    	public function listarItens()
    		{
    		$limites = array (10,25,50);
    		$id = 0;
    		if ($this->request->idtb_aspiracao)
    			{
    			$id = $this->request->idtb_aspiracao;
    			}
    		echo ($id);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
    		$aspiracoes = Aspiracao::find_list($id,$pagina,$limite);
    		$visao = new Visao();
       	$visao->visualizar("cadastro_fiv_list_aspiracao");
       	$visao->setVariable("idtb_aspiracao",$id);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	$i=0;
       	foreach ($aspiracoes as $aspiracao) 
            {
       		$visao->setCurrentBlock("DADOSASP");
       		$visao->setVariable("aspiracao",$aspiracao->idtb_aspiracao);
       		$propriedade = Propriedade::find($aspiracao->tb_propriedade_idtb_propriedade);
       		$visao->setVariable("propriedade",$propriedade->nome_propriedade);
       		$cliente = Cliente::find($propriedade->tb_cliente_idtb_cliente);
       		$visao->setVariable("cliente",$cliente->nome_cliente);
       		$visao->parseCurrentBlock("DADOSASP");
       		$i++;
       		}
       	$visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Aspiracao::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Aspiracao::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->setVariable("idtb_aspiracao",$id);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->id;
        	$Aspiracao = Semen::find($id);
			$visao = new Visao();
       	$visao->visualizar("cadastro_fiv");
       	$visao->setVariable("acao","salvar");
       	$visao->setVariable("idtb_semen",$Aspiracao->idtb_semen);
       	$visao->setVariable("tb_qntidade_Aspiracao",$Aspiracao->tb_qntidade_Aspiracao);
       	$touros = Touro::all();
         foreach ($touros as $touro)
         	{
         	$visao->setCurrentBlock("TOUROS");
         	$visao->setVariable("idtb_touro",$touro->idtb_touro);
         	if ($Aspiracao->tb_touro_idtb_touro == $touro->idtb_touro)
         		{
         		$visao->setVariable("selecionado","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionado","");
         		}
         	$visao->setVariable("nome_touro",$touro->nome_touro);
         	$visao->parseCurrentBlock("TOUROS");
         	}
         $clientes = Cliente::all();
         foreach ($clientes as $cliente)
         	{
         	$visao->setCurrentBlock("CLIENTES");
         	$visao->setVariable("idtb_cliente",$cliente->idtb_cliente);
         	if ($Aspiracao->tb_cliente_idtb_cliente == $cliente->idtb_cliente)
         		{
         		$visao->setVariable("selecionados","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selecionados","");
         		}
         	$visao->setVariable("nome_cliente",$cliente->nome_cliente);
         	$visao->parseCurrentBlock("CLIENTES");
         	}
         //return $this->view('form', ['contato' => $contato]);
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
    		var_dump($this->request);
    		$fiv_item = new FIV;
    		$fiv_item->idtb_fiv = $this->request->idtb_fiv;
    		$fiv_item->tb_doadora_touro_idtb_doadora_touro = $this->request->idtb_doadora_touro;
    		$fiv_item->total_civ_fiv = $this->request->total_civ_fiv;
    		$fiv_item->clivados_fiv = $this->request->clivados_fiv;
    		$fiv_item->mor_fiv = $this->request->mor_fiv;
    		$fiv_item->bi_fiv = $this->request->bi_fiv;
    		$fiv_item->bl_fiv = $this->request->bl_fiv;
    		$fiv_item->bx_fiv = $this->request->bx_fiv;
    		$fiv_item->total_embrioes_fiv = $this->request->total_embrioes_fiv;
    		$fiv_item->percentual_fiv = $this->request->percentual_fiv;
    		$fiv_item->te_fiv = $this->request->te_fiv;
    		$fiv_item->vitr_fiv = $this->request->vitr_fiv;
    		$fiv_item->data_fechamento_fiv = $this->request->data_fechamento_fiv;
        	if ($fiv_item->save(0)) 
        		{
            echo ("Registrado");
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
    		//var_dump($this->request);
    		$idfiv = $this->request->idtb_fiv;
    		$idtb_doadora_touro = $this->request->idtb_doadora_touro;
    		$fiv_item = FIV::registro($idfiv,$idtb_doadora_touro);
    		$fiv_item->total_civ_fiv = $this->request->total_civ_fiv;
    		$fiv_item->clivados_fiv = $this->request->clivados_fiv;
    		$fiv_item->mor_fiv = $this->request->mor_fiv;
    		$fiv_item->bi_fiv = $this->request->bi_fiv;
    		$fiv_item->bl_fiv = $this->request->bl_fiv;
    		$fiv_item->bx_fiv = $this->request->bx_fiv;
    		$fiv_item->total_embrioes_fiv = $this->request->total_embrioes_fiv;
    		$fiv_item->percentual_fiv = $this->request->percentual_fiv;
    		$fiv_item->te_fiv = $this->request->te_fiv;
    		$fiv_item->vitr_fiv = $this->request->vitr_fiv;
    		$fiv_item->data_fechamento_fiv = $this->request->data_fechamento_fiv;
    		$rows = $fiv_item->save(1);
    		//echo ("linhas afetadas: ".$rows);
        	if ($rows > 0) 
        		{
            echo ("Actualizado");
        		}
        	else 
        		{
        		echo ("Datos no modificados");
        		}
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->id;
        	$Aspiracao = Semen::destroy($id);
        	return $this->listar();
    		}

		};