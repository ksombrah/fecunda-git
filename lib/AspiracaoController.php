<?php

	class AspiracaoController extends Controller
		{

    	/**
     	 * Lista de Usuários
       */
    	public function listar()
    		{
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->pagina)
    			{
    			$pagina = $this->request->pagina;
    			}
    		$limite = 10;
    		if ($this->request->limite)
    			{
    			$limite = $this->request->limite;
    			}
        	$Aspiracoes = Aspiracao::all($pagina,$limite);
       	$visao = new Visao();
       	$visao->visualizar("lista_aspiracao");
       	$visao->setVariable("pagina",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
       	/*if (strcmp(trim($acesso->priv_admin),"Y") == 0 )
       		{
       		$visao->setVariable("ativon","");
       		}
       	else 
       		{
       		$visao->setVariable("ativon","style=\"visibility:hidden;\"");
       		}*/
       	$i=0;
       	foreach ($Aspiracoes as $aspiracao) 
            {
            $visao->setCurrentBlock("DADOS");
            $visao->setVariable("idtb_aspiracao",$aspiracao->idtb_aspiracao);
            $visao->setVariable("hora_inicio_aspiracao",$aspiracao->hora_inicio_aspiracao);
            $visao->setVariable("hora_fim_aspiracao",$aspiracao->hora_fim_aspiracao);
            $propriedade = Propriedade::find($aspiracao->tb_propriedade_idtb_propriedade);
            $visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
            $visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
         $visao->setVariable("primeiro",1);
         $visao->setVariable("contagem",$i);
         $visao->setVariable("total",Aspiracao::count());
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_a","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_a","");
         	}
         $visao->setVariable("pagina_a",$ant);
         $visao->setVariable("limite_a",$limite);
         $total_p = ceil(Aspiracao::count()/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("active","active");
         		}
         	else 
         		{
         		$visao->setVariable("active","");
         		}
            $visao->setVariable("pagina_n",$p);
            $visao->setVariable("limite_n",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_p","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_p","disabled");
         	}
         $visao->setVariable("pagina_p",$prox);
         $visao->setVariable("limite_p",$limite);
        	//return $this->view('grade', ['contatos' => $contatos]);
    		}
    		
    	public function listarItens()
    		{
    		$id = $this->request->idtb_aspiracao;
    		$limites = array (10,25,50);
    		$acesso = unserialize($_SESSION['usuario']);
    		//var_dump($acesso);
    		$pagina = 1;
    		if ($this->request->paginav)
    			{
    			$pagina = $this->request->paginav;
    			}
    		$limite = 10;
    		if ($this->request->limitev)
    			{
    			$limite = $this->request->limitev;
    			}
    		$AspiracoesItens = AspiracaoItens::all($pagina,$limite,$id);
    		$visao = new Visao();
    		$visao->visualizar("lista_aspiracao_visualizacao");
       	$visao->setVariable("paginav",$pagina);
       	for ($l = 0; $l < count($limites); $l++)
       		{
       		$visao->setCurrentBlock("LIMITES");
       		$visao->setVariable("op_limite",$limites[$l]);
         	if ($limites[$l] == $limite)
         		{
         		$visao->setVariable("selected","selected");
         		}
         	else 
         		{
         		$visao->setVariable("selected","");
         		}
            $visao->parseCurrentBlock("LIMITES");
       		}
    		$visao->setVariable("idtb_aspiracao",$id);
    		$i=0;
    		foreach ($AspiracoesItens as $itens) 
            {
            $visao->setCurrentBlock("DADOS");
            $doadora = Doadora::find($itens->tb_doadora_idtb_doadora);
            $visao->setVariable("arp_doadora",$doadora->arp_doadora);
            $visao->setVariable("ocitos_grau_I_aspiracao",$itens->ocitos_grau_I_aspiracao);
            $visao->setVariable("ocitos_grau_II_aspiracao",$itens->ocitos_grau_II_aspiracao);
            $visao->setVariable("ocitos_grau_III_aspiracao",$itens->ocitos_grau_III_aspiracao);
            $visao->setVariable("ocitos_viaveis_aspiracao",$itens->ocitos_viaveis_aspiracao);
            $visao->setVariable("ocitos_nao_viaveis_aspiracao",$itens->ocitos_nao_viaveis_aspiracao);
            $visao->setVariable("total_ocitos_aspiracao",$itens->total_ocitos_aspiracao);
            $touro = Touro::find($itens->tb_touro_idtb_touro);
            $visao->setVariable("nome_touro",$touro->nome_touro);
            $visao->setVariable("semem_aspiracao",$itens->semem_aspiracao);
            $visao->parseCurrentBlock("DADOS");
           	$i++;          	
            }
    		$visao->setVariable("primeirov",1);
         $visao->setVariable("contagemv",$i);
         $visao->setVariable("totalv",AspiracaoItens::count($id));
         $ant = $pagina-1;
         if ($ant < 1)
         	{
         	$visao->setVariable("disabled_av","disabled");
         	$ant = 1;
         	}
         else 
         	{
         	$visao->setVariable("disabled_av","");
         	}
         $visao->setVariable("pagina_av",$ant);
         $visao->setVariable("limite_av",$limite);
         $total_p = ceil(AspiracaoItens::count($id)/$limite);
         for ($p=1; $p <= $total_p; $p++)
         	{
         	$visao->setCurrentBlock("PAGINAS");
         	if ($p == $pagina)
         		{
         		$visao->setVariable("activev","active");
         		}
         	else 
         		{
         		$visao->setVariable("activev","");
         		}
            $visao->setVariable("pagina_nv",$p);
            $visao->setVariable("limite_nv",$limite);
            $visao->parseCurrentBlock("PAGINAS");
         	}
         if ($pagina < $total_p)
         	{
         	$prox = $pagina+1;
         	$visao->setVariable("disabled_pv","");
         	}
         else 
         	{
         	$prox = $total_p;
         	$visao->setVariable("disabled_pv","disabled");
         	}
         $visao->setVariable("pagina_pv",$prox);
         $visao->setVariable("limite_pv",$limite);
    		}

    	/**
       * Mostrar formulario para criar um novo contato
       */
    	public function criar()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("cadastro_aspiracao");
       	$visao->setVariable("acao","salvar");
       	$visao->setVariable("acao_txt","Registrar");
       	$visao->setVariable("n",1);
       	$propriedades = Propriedade::all();
         foreach ($propriedades as $propriedade)
         	{
         	$visao->setCurrentBlock("PROPRIEDADES");
         	$visao->setVariable("idtb_propriedade",$propriedade->idtb_propriedade);
         	$visao->setVariable("selecionado","");
         	$visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
         	$visao->parseCurrentBlock("PROPRIEDADES");
         	}
         $doadoras = Doadora::all();
         foreach ($doadoras as $doadora)
         	{
         	$visao->setCurrentBlock("DOADORA");
         	$visao->setVariable("idtb_doadora",$doadora->idtb_doadora);
         	$visao->setVariable("selecionado","");
         	$propriedade = Propriedade::find($doadora->tb_propriedade_idtb_propriedade);
         	$descricao = $doadora->idtb_doadora." - ".$propriedade->nome_propriedade;
         	$visao->setVariable("descricao",$descricao);
         	$visao->parseCurrentBlock("DOADORA");
         	}
         $touros = Touro::all();
         foreach ($touros as $touro)
         	{
         	$visao->setCurrentBlock("TOURO");
         	$visao->setVariable("idtb_touro",$touro->idtb_touro);
         	$visao->setVariable("selecionado","");
         	$visao->setVariable("descricao",$touro->nome_touro);
         	$visao->parseCurrentBlock("TOURO");
         	}
    		}
    		
    	public function criaritem()
    		{
    		//modificar depois
    		$visao = new Visao();
       	$visao->visualizar("aspiracao_item");
       	$visao->setVariable("n",$this->request->n+1);
         $doadoras = Doadora::all();
         foreach ($doadoras as $doadora)
         	{
         	$visao->setCurrentBlock("DOADORA");
         	$visao->setVariable("idtb_doadora",$doadora->idtb_doadora);
         	$visao->setVariable("selecionado","");
         	$propriedade = Propriedade::find($doadora->tb_propriedade_idtb_propriedade);
         	$descricao = $doadora->idtb_doadora." - ".$propriedade->nome_propriedade;
         	$visao->setVariable("descricao",$descricao);
         	$visao->parseCurrentBlock("DOADORA");
         	}
         $touros = Touro::all();
         foreach ($touros as $touro)
         	{
         	$visao->setCurrentBlock("TOURO");
         	$visao->setVariable("idtb_touro",$touro->idtb_touro);
         	$visao->setVariable("selecionado","");
         	$visao->setVariable("descricao",$touro->nome_touro);
         	$visao->parseCurrentBlock("TOURO");
         	}
    		}

    	/**
       * Mostrar formulário para editar um contato
       */
    	public function editar()
    		{
        	$id      = $this->request->idtb_aspiracao;
        	$Aspiracao = Aspiracao::find($id);
			$visao = new Visao();
       	$visao->visualizar("cadastro_aspiracao");
       	$visao->setVariable("acao","atualizar");
       	$visao->setVariable("acao_txt","Registrar");
       	$visao->setVariable("idtb_aspiracao",$Aspiracao->idtb_aspiracao);
       	$visao->setVariable("hora_inicio_aspiracao",$Aspiracao->hora_inicio_aspiracao);
       	$visao->setVariable("hora_fim_aspiracao",$Aspiracao->hora_fim_aspiracao);
       	$propriedades = Propriedade::all();
       	foreach ($propriedades as $propriedade)
         	{
         	$visao->setCurrentBlock("PROPRIEDADES");
         	$visao->setVariable("idtb_propriedade",$propriedade->idtb_propriedade);
         	if ($propriedade->idtb_propriedade == $Aspiracao->tb_propriedade_idtb_propriedade)
         		{
         		$visao->setVariable("selecionado","selected");
         		}
         	else
         		{
         		$visao->setVariable("selecionado","");
         		}
         	$visao->setVariable("nome_propriedade",$propriedade->nome_propriedade);
         	$visao->parseCurrentBlock("PROPRIEDADES");
         	}
    		}

    	/**
       * Salvar o contato submetido pelo formulário
       */
    	public function salvar()
    		{
    		$ok = false;
    		//Debugação
    		/*echo ("Request: ");
    		print_r($this->request);
    		echo ("</br>POST:");
    		print_r($_POST);
    		*/
        	$Aspiracao           = new Aspiracao;
        	$Aspiracao->hora_inicio_aspiracao = $this->request->hora_inicio_aspiracao; ;
        	$Aspiracao->hora_fim_aspiracao = $this->request->hora_fim_aspiracao;
        	$Aspiracao->tb_propriedade_idtb_propriedade = $this->request->tb_propriedade_idtb_propriedade;
        	if ($Aspiracao->save(0)) 
        		{
        		$itens = count($this->request->tb_doadora_idtb_doadora);
        		for ($i=0; $i < $itens; $i++)
        			{
            	$AspiracaoItens = new AspiracaoItens;
            	$AspiracaoItens->tb_aspiracao_idtb_aspiracao = $Aspiracao->idtb_aspiracao;
            	$AspiracaoItens->tb_doadora_idtb_doadora = $this->request->tb_doadora_idtb_doadora[$i];
            	$AspiracaoItens->ocitos_grau_I_aspiracao = $this->request->ocitos_grau_I_aspiracao[$i];
            	$AspiracaoItens->ocitos_grau_II_aspiracao = $this->request->ocitos_grau_II_aspiracao[$i];
            	$AspiracaoItens->ocitos_grau_III_aspiracao = $this->request->ocitos_grau_III_aspiracao[$i];
            	$AspiracaoItens->ocitos_viaveis_aspiracao = $this->request->ocitos_viaveis_aspiracao[$i];
            	$AspiracaoItens->ocitos_nao_viaveis_aspiracao = $this->request->ocitos_nao_viaveis_aspiracao[$i];
            	$AspiracaoItens->total_ocitos_aspiracao = $this->request->total_ocitos_aspiracao[$i];
            	$AspiracaoItens->tb_touro_idtb_touro = $this->request->tb_touro_idtb_touro[$i];
            	$AspiracaoItens->semem_aspiracao = $this->request->semem_aspiracao[$i];
            	if ($AspiracaoItens->save(0)) 
        				{
        				//Inserir item no FIV
        				$FIV = new FIV;
        				$FIV->tb_doadora_touro_idtb_doadora_touro = $AspiracaoItens->idtb_doadora_touro;
        				if ($FIV->save(0))
        					{
           				$ok = true;
           				}
        				}
            	}
        		}
    		}

    	/**
     	 * Atualizar o contato conforme dados submetidos
     	 */
    	public function atualizar()
    		{
        	$id                = $this->request->idtb_semen;
        	$Aspiracao           = Semen::find($id);
        	$Aspiracao->idtb_semen = $this->request->idtb_semen;
        	$Aspiracao->tb_touro_idtb_touro = $this->request->tb_touro_idtb_touro;
        	$Aspiracao->tb_cliente_idtb_cliente = $this->request->tb_cliente_idtb_cliente;
        	$Aspiracao->tb_qntidade_Aspiracao = $this->request->tb_qntidade_Aspiracao;
        	$Aspiracao->save(1);

        	return $this->listar();
    		}

    	/**
       * Apagar um contato conforme o id informado
       */
    	public function excluir()
    		{
        	$id      = $this->request->id;
        	$Aspiracao = Semen::destroy($id);
        	return $this->listar();
    		}

		};