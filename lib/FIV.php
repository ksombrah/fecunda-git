<?php
	
	class FIV extends Funcoes
		{
		private $atributos;
		
		public function __construct()
			{
			}
			
		public function __destruct()
			{
			}
			
		public function __set(string $atributo, $valor)
	    	{
	      $this->atributos[$atributo] = $valor;
	      return $this;
	    	}
	
	   public function __get(string $atributo)
	   	{
	      return $this->atributos[$atributo];
	    	}
	
	   public function __isset($atributo)
	    	{
	      return isset($this->atributos[$atributo]);
	    	}
	   
	   /**
	     * Salvar o contato
	     * @return boolean
	     */
	   public function save($modo)
    		{
        	$colunas = $this->preparar($this->atributos);
        	if ($modo == 0) 
        		{
            $query = "INSERT INTO tb_fiv (".
                implode(', ', array_keys($colunas)).
                ") VALUES (".
                implode(', ', array_values($colunas)).");";
        		} 
        	else 
        		{
            foreach ($colunas as $key => $value) 
            	{
               if (($key !== 'idtb_fiv')&&($key !== 'tb_doadora_touro_idtb_doadora_touro'))
               	{
                  $definir[] = "{$key}={$value}";
                	}
            	}
            $query = "UPDATE tb_fiv SET ".implode(', ', $definir)." WHERE idtb_fiv={$this->idtb_fiv} and tb_doadora_touro_idtb_doadora_touro={$this->tb_doadora_touro_idtb_doadora_touro};";
        		}
        	//echo ($query);
        	if ($conexao = Conexao::getInstance()) 
        		{
            return $conexao->exec($query);
        		}
        	return false;
    		}

    	/**
       * Retorna uma lista de contatos
       * @return array/boolean
       */
    	public static function all($pagina=false,$limite=false)
    		{
        	$conexao = Conexao::getInstance();
        	$sql = "SELECT * FROM tb_fiv order by idtb_fiv";
        	if ($pagina)
        		{
        		$offset = $limite*($pagina-1);
        		$sql .= " limit $offset,$limite "; 
        		}
        	//echo ($sql);
        	$stmt    = $conexao->prepare($sql);
        	$result  = array();
        	if ($stmt->execute()) 
        		{
            while ($rs = $stmt->fetchObject(FIV::class)) 
            	{
               $result[] = $rs;
            	}
        		}
        	if (count($result) > 0) 
        		{
            return $result;
        		}
        	return false;
    		}

    	/**
       * Retornar o número de registros
       * @return int/boolean
       */
    	public static function count()
    		{
        	$conexao = Conexao::getInstance();
        	$count   = $conexao->prepare("SELECT count(*) FROM tb_fiv;");
        	if ($count->execute()) 
        		{
        		$dd = $count->fetchAll();
            return (int) $dd[0][0];
        		}
        	return false;
    		}

    	/**
       * Encontra um recurso pelo id
       * @param type $id
       * @return type
       */
    	public static function find($id)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT * FROM tb_fiv WHERE idtb_fiv={$id};";
        	//echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute()) 
        		{
            if ($stmt->rowCount() > 0) 	
            	{
               $resultado = $stmt->fetchObject(FIV::class);
               if ($resultado) 	
               	{
                  return $resultado;
                	}
            	}
        		}
        	return false;
    		}
    	/**
       * Encontra um recurso pelo id
       * @param type $id
       * @return type
       */
    	public static function registro($idtb_aspiracao,$idtb_doadora_touro)
    		{
        	$conexao = Conexao::getInstance();
        	$consulta = "SELECT * FROM tb_fiv WHERE idtb_fiv={$idtb_aspiracao} and tb_doadora_touro_idtb_doadora_touro={$idtb_doadora_touro};";
        	//echo ($consulta);
        	$stmt = $conexao->prepare($consulta);
        	if ($stmt->execute()) 
        		{
            if ($stmt->rowCount() > 0) 	
            	{
               $resultado = $stmt->fetchObject(FIV::class);
               if ($resultado) 	
               	{
                  return $resultado;
                	}
            	}
        		}
        	return false;
    		}
    		
    	/**
       * Destruir um recurso
       * @param type $id
       * @return boolean
       * Rever esse método questão: temporiedade 
       */
    	public static function destroy($id)
    		{
        	$conexao = Conexao::getInstance();
        	if ($conexao->exec("DELETE FROM tb_fiv WHERE idtb_fiv={$id};")) 
        		{
            return true;
        		}
        	return false;
    		}
		};
?>