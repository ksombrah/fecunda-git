<?php
	//Iniciando a sessão:
	if (session_status() !== PHP_SESSION_ACTIVE) 
		{
		//Definindo o prazo para a cache expirar em 1 minutos.
  		session_cache_expire(1);
  		session_start();
		}
	error_reporting(E_ALL);
	ini_set('display_errors', true);

	spl_autoload_register(function($class) 
		{
   	if (file_exists("lib/$class.php")) 
   		{
      	require_once "lib/$class.php";
      	return true;
    		}
		});
	$pagina = new Visao();
	if ($_GET) 
		{
		$controller = isset($_GET['controller']) ? ((class_exists($_GET['controller'])) ? new $_GET['controller'] : NULL ) : null;
		$method     = isset($_GET['method']) ? $_GET['method'] : null;
      if ($controller && $method) 
      	{
         if (method_exists($controller, $method)) 
         	{
            $parameters = $_GET;
            unset($parameters['controller']);
            unset($parameters['method']);
            call_user_func(array($controller, $method), $parameters);
            } 
         else 
         	{
            $msg = "Método não encontrado!";
            }
         } 
      else 
      	{
         $msg = "Controller não encontrado!";
         }
      } 
   else 
   	{
      $msg = "Página Inicial";
      }
   $pagina->mensagem = $msg;
   if ( isset($_SESSION['login']) )
   	{
   	if ($_SESSION['login'] == 0)
   		{
   		$pagina->titulo_pg = "Fecunda Biotecnologia";
   		$pagina->visualizar();
   		}
  		else 
   		{
   		$pagina->titulo_pg = "Sitema de Gestión Fecunda Biotecnologia";
   		$pagina->visualizar("inicio");
   		}
   	}
   else 
		{
		$pagina->titulo_pg = "Fecunda Biotecnologia";
		$pagina->visualizar();
		}
?>